#!/bin/bash

PROG=$0
PREFIX=$HOME/.local
UNINSTALL="no"
DRYRUN=""

while [ "$#" -gt 0 ]
do
	case "$1" in
		--prefix)
			shift 1
			PREFIX=$1
			echo "New installation prefix:" $PREFIX
			shift 1
			;;

		--uninstall)
			UNINSTALL="yes"
			shift 1
			;;

		--dry-run)
			DRYRUN=echo
			shift 1
			;;

		-h|--help)
			echo -e "$PROG [options]"
			echo -e "Options:"
			echo -e "\t-h, --help    : show this message."
			echo -e "\t--prefix <dir>: directory into which the soft will be installed."
			echo -e "\t--uninstall   : uninstall the commands in PREFIX."
			echo -e "\t--dry-run     : show command instead of executing them."
			exit 0
			;;
	esac
done

if [[ "$UNINSTALL" = "no" ]]
then
	echo "Installing in" $PREFIX

	for f in bin/*
	do
		$DRYRUN install -Dm 755 $f $PREFIX/bin/$(basename $f)
	done
else
	echo "Uninstalling from" $PREFIX

	for f in bin/*
	do
		if [[ -f "$PREFIX/bin/$(basename $f)" ]]
		then
			$DRYRUN rm $PREFIX/bin/$(basename $f)
		fi
	done
fi
