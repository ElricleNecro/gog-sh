# GOG

This is a basic CLI interface to the cli [lgogdownloader](https://github.com/Sude-/lgogdownloader).

## Why a CLI front-end to another CLI program?

For the fun of learning stuff, playing around with code and because I found
lgogdownloader to be a very powerful and complete CLI to
[GoG](https://www.gog.com), but it is a little bit cumbersome to use to deal
with big game library, (typically install games and updating them).

## How to install?

Simple, just do:
```bash
install.sh
```
by default, it will install in `$HOME/.local`. If you want to change the installation prefix, run:
```bash
install.sh --prefix <installation-prefix>
```

## What if I am not happy with and want to remove it?

If you are sure about it, you can use:
```bash
install.sh --uninstall
```
If, when installing, you gave the `--prefix` parameter, you will have to give it again.

## Why write it in bash?

Because it seems more natural to write program calling others program in shell. But I am thinking to write a version in python.
